object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #1056#1072#1079#1074#1077#1076#1092#1086#1088#1084#1072
  ClientHeight = 616
  ClientWidth = 696
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    696
    616)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 29
    Top = 16
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 67
    Top = 16
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 110
    Top = 16
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 150
    Top = 16
    Width = 14
    Height = 13
    Caption = '*2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 190
    Top = 16
    Width = 13
    Height = 13
    Caption = '/2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 232
    Top = 16
    Width = 23
    Height = 13
    Caption = '+60'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 276
    Top = 16
    Width = 23
    Height = 13
    Caption = '+50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 315
    Top = 16
    Width = 23
    Height = 13
    Caption = '+40'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 354
    Top = 16
    Width = 23
    Height = 13
    Caption = '+30'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 393
    Top = 16
    Width = 19
    Height = 13
    Caption = '-60'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 432
    Top = 16
    Width = 19
    Height = 13
    Caption = '-50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 471
    Top = 16
    Width = 19
    Height = 13
    Caption = '-40'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 510
    Top = 16
    Width = 19
    Height = 13
    Caption = '-30'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 28
    Top = 326
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 71
    Top = 326
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label16: TLabel
    Left = 114
    Top = 326
    Width = 21
    Height = 13
    Caption = '100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label17: TLabel
    Left = 154
    Top = 326
    Width = 14
    Height = 13
    Caption = '*2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label18: TLabel
    Left = 194
    Top = 326
    Width = 13
    Height = 13
    Caption = '/2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label19: TLabel
    Left = 236
    Top = 326
    Width = 23
    Height = 13
    Caption = '+60'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 280
    Top = 326
    Width = 23
    Height = 13
    Caption = '+50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 319
    Top = 326
    Width = 23
    Height = 13
    Caption = '+40'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label22: TLabel
    Left = 358
    Top = 326
    Width = 23
    Height = 13
    Caption = '+30'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 397
    Top = 326
    Width = 19
    Height = 13
    Caption = '-60'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label24: TLabel
    Left = 436
    Top = 326
    Width = 19
    Height = 13
    Caption = '-50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label25: TLabel
    Left = 475
    Top = 326
    Width = 19
    Height = 13
    Caption = '-40'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label26: TLabel
    Left = 514
    Top = 326
    Width = 19
    Height = 13
    Caption = '-30'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label27: TLabel
    Left = 63
    Top = 404
    Width = 141
    Height = 23
    Caption = #1052#1086#1103' '#1082#1072#1088#1090#1072' '#1085#1072' '#1088#1091#1082#1072#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label28: TLabel
    Left = 63
    Top = 444
    Width = 176
    Height = 23
    Caption = #1052#1086#1103' '#1082#1072#1088#1090#1072' '#1087#1088#1077#1076#1098#1103#1074#1083#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label29: TLabel
    Left = 63
    Top = 484
    Width = 171
    Height = 23
    Caption = #1045#1075#1086' '#1082#1072#1088#1090#1072' '#1087#1088#1077#1076#1098#1103#1074#1083#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label30: TLabel
    Left = 287
    Top = 364
    Width = 110
    Height = 23
    Caption = #1042#1086#1079#1084#1086#1078#1085#1086' '#1077#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label31: TLabel
    Left = 287
    Top = 404
    Width = 161
    Height = 23
    Caption = #1055#1086#1095#1090#1080' '#1085#1072#1074#1077#1088#1085#1103#1082#1072' '#1077#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label32: TLabel
    Left = 287
    Top = 444
    Width = 80
    Height = 23
    Caption = #1058#1086#1095#1085#1086' '#1077#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label33: TLabel
    Left = 496
    Top = 364
    Width = 103
    Height = 23
    Caption = #1042#1086#1079#1084#1086#1078#1085#1086' '#1085#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label34: TLabel
    Left = 496
    Top = 404
    Width = 154
    Height = 23
    Caption = #1055#1086#1095#1090#1080' '#1085#1072#1074#1077#1088#1085#1103#1082#1072' '#1085#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label35: TLabel
    Left = 496
    Top = 444
    Width = 73
    Height = 23
    Caption = #1058#1086#1095#1085#1086' '#1085#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label36: TLabel
    Left = 287
    Top = 473
    Width = 155
    Height = 46
    Caption = #1045#1089#1090#1100' '#1080#1083#1080' '#1101#1090#1072' '#1080#1083#1080' '#1076#1088#1091#1075#1072#1103' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label37: TLabel
    Left = 287
    Top = 516
    Width = 155
    Height = 46
    Caption = #1045#1089#1090#1100' '#1080#1083#1080' '#1101#1090#1072' '#1080#1083#1080' '#1076#1088#1091#1075#1072#1103' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label38: TLabel
    Left = 287
    Top = 559
    Width = 155
    Height = 46
    Caption = #1045#1089#1090#1100' '#1080#1083#1080' '#1101#1090#1072' '#1080#1083#1080' '#1076#1088#1091#1075#1072#1103' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label39: TLabel
    Left = 496
    Top = 470
    Width = 156
    Height = 46
    Caption = #1053#1077#1090' '#1080#1083#1080' '#1101#1090#1086#1081' '#1080#1083#1080' '#1076#1088#1091#1075#1086#1081' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label40: TLabel
    Left = 496
    Top = 516
    Width = 156
    Height = 46
    Caption = #1053#1077#1090' '#1080#1083#1080' '#1101#1090#1086#1081' '#1080#1083#1080' '#1076#1088#1091#1075#1086#1081' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label41: TLabel
    Left = 496
    Top = 559
    Width = 156
    Height = 46
    Caption = #1053#1077#1090' '#1080#1083#1080' '#1101#1090#1086#1081' '#1080#1083#1080' '#1076#1088#1091#1075#1086#1081' '#1089' '#1101#1090#1080#1084' '#1079#1085#1072#1082#1086#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label42: TLabel
    Left = 63
    Top = 524
    Width = 164
    Height = 23
    Caption = #1053#1077#1090' '#1087#1086' '#1087#1088#1072#1074#1080#1083#1072#1084' '#1080#1075#1088#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label44: TLabel
    Left = 28
    Top = 359
    Width = 192
    Height = 23
    Caption = #1059#1089#1083#1086#1074#1085#1099#1077' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label45: TLabel
    Left = 63
    Top = 559
    Width = 118
    Height = 23
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1082#1083#1077#1090#1082#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    OnClick = Panel53Click
  end
  object Panel1: TPanel
    Tag = 1
    Left = 24
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    OnClick = Panel1Click
  end
  object Panel2: TPanel
    Tag = 2
    Left = 63
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    OnClick = Panel1Click
  end
  object Panel3: TPanel
    Tag = 3
    Left = 102
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    OnClick = Panel1Click
  end
  object Panel4: TPanel
    Tag = 4
    Left = 141
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    OnClick = Panel1Click
  end
  object Panel5: TPanel
    Tag = 5
    Left = 180
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    OnClick = Panel1Click
  end
  object Panel6: TPanel
    Left = 232
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 5
    OnClick = Panel1Click
  end
  object Panel7: TPanel
    Left = 271
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 6
    OnClick = Panel1Click
  end
  object Panel8: TPanel
    Left = 310
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 7
    OnClick = Panel1Click
  end
  object Panel9: TPanel
    Left = 349
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    OnClick = Panel1Click
  end
  object Panel10: TPanel
    Left = 388
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 9
    OnClick = Panel1Click
  end
  object Panel11: TPanel
    Left = 427
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 10
    OnClick = Panel1Click
  end
  object Panel12: TPanel
    Left = 466
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 11
    OnClick = Panel1Click
  end
  object Panel13: TPanel
    Left = 505
    Top = 40
    Width = 33
    Height = 65
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 12
    OnClick = Panel1Click
  end
  object Panel14: TPanel
    Left = 24
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 13
    OnClick = Panel1Click
  end
  object Panel15: TPanel
    Left = 63
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 14
    OnClick = Panel1Click
  end
  object Panel16: TPanel
    Left = 102
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 15
    OnClick = Panel1Click
  end
  object Panel17: TPanel
    Left = 141
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 16
    OnClick = Panel1Click
  end
  object Panel18: TPanel
    Left = 180
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 17
    OnClick = Panel1Click
  end
  object Panel19: TPanel
    Left = 232
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 18
    OnClick = Panel1Click
  end
  object Panel20: TPanel
    Left = 271
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 19
    OnClick = Panel1Click
  end
  object Panel21: TPanel
    Left = 310
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 20
    OnClick = Panel1Click
  end
  object Panel22: TPanel
    Left = 349
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 21
    OnClick = Panel1Click
  end
  object Panel23: TPanel
    Left = 388
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 22
    OnClick = Panel1Click
  end
  object Panel24: TPanel
    Left = 427
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 23
    OnClick = Panel1Click
  end
  object Panel25: TPanel
    Left = 466
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 24
    OnClick = Panel1Click
  end
  object Panel26: TPanel
    Left = 505
    Top = 111
    Width = 33
    Height = 65
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 25
    OnClick = Panel1Click
  end
  object Panel27: TPanel
    Left = 24
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 26
    OnClick = Panel1Click
  end
  object Panel28: TPanel
    Left = 63
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 27
    OnClick = Panel1Click
  end
  object Panel29: TPanel
    Left = 102
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 28
    OnClick = Panel1Click
  end
  object Panel30: TPanel
    Left = 141
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 29
    OnClick = Panel1Click
  end
  object Panel31: TPanel
    Left = 180
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 30
    OnClick = Panel1Click
  end
  object Panel32: TPanel
    Left = 232
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 31
    OnClick = Panel1Click
  end
  object Panel33: TPanel
    Left = 271
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 32
    OnClick = Panel1Click
  end
  object Panel34: TPanel
    Left = 310
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 33
    OnClick = Panel1Click
  end
  object Panel35: TPanel
    Left = 349
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 34
    OnClick = Panel1Click
  end
  object Panel36: TPanel
    Left = 388
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 35
    OnClick = Panel1Click
  end
  object Panel37: TPanel
    Left = 427
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 36
    OnClick = Panel1Click
  end
  object Panel38: TPanel
    Left = 466
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 37
    OnClick = Panel1Click
  end
  object Panel39: TPanel
    Left = 505
    Top = 184
    Width = 33
    Height = 65
    Color = clYellow
    ParentBackground = False
    TabOrder = 38
    OnClick = Panel1Click
  end
  object Panel40: TPanel
    Left = 24
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 39
    OnClick = Panel1Click
  end
  object Panel41: TPanel
    Left = 63
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 40
    OnClick = Panel1Click
  end
  object Panel42: TPanel
    Left = 102
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 41
    OnClick = Panel1Click
  end
  object Panel43: TPanel
    Left = 141
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 42
    OnClick = Panel1Click
  end
  object Panel44: TPanel
    Left = 180
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 43
    OnClick = Panel1Click
  end
  object Panel45: TPanel
    Left = 232
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 44
    OnClick = Panel1Click
  end
  object Panel46: TPanel
    Left = 271
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 45
    OnClick = Panel1Click
  end
  object Panel47: TPanel
    Left = 310
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 46
    OnClick = Panel1Click
  end
  object Panel48: TPanel
    Left = 349
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 47
    OnClick = Panel1Click
  end
  object Panel49: TPanel
    Left = 388
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 48
    OnClick = Panel1Click
  end
  object Panel50: TPanel
    Left = 427
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 49
    OnClick = Panel1Click
  end
  object Panel51: TPanel
    Left = 466
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 50
    OnClick = Panel1Click
  end
  object Panel52: TPanel
    Left = 505
    Top = 255
    Width = 33
    Height = 65
    Color = clGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 51
    OnClick = Panel1Click
  end
  object Panel53: TPanel
    Left = 24
    Top = 399
    Width = 33
    Height = 34
    Caption = 'H'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 52
    OnClick = Panel53Click
  end
  object Panel54: TPanel
    Left = 24
    Top = 439
    Width = 33
    Height = 34
    Caption = 'S'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 53
    OnClick = Panel53Click
  end
  object Panel55: TPanel
    Left = 24
    Top = 479
    Width = 33
    Height = 34
    Caption = 'U'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 54
    OnClick = Panel53Click
  end
  object Panel56: TPanel
    Left = 248
    Top = 359
    Width = 33
    Height = 34
    Caption = '?'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 55
    OnClick = Panel53Click
  end
  object Panel57: TPanel
    Left = 248
    Top = 399
    Width = 33
    Height = 34
    Caption = '?!'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 56
    OnClick = Panel53Click
  end
  object Panel58: TPanel
    Left = 248
    Top = 439
    Width = 33
    Height = 34
    Caption = '!'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 57
    OnClick = Panel53Click
  end
  object Panel59: TPanel
    Left = 457
    Top = 359
    Width = 33
    Height = 34
    Caption = 'X?'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 58
    OnClick = Panel53Click
  end
  object Panel60: TPanel
    Left = 457
    Top = 399
    Width = 33
    Height = 34
    Caption = 'X?!'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 59
    OnClick = Panel53Click
  end
  object Panel61: TPanel
    Left = 457
    Top = 439
    Width = 33
    Height = 34
    Caption = 'X!'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 60
    OnClick = Panel53Click
  end
  object Panel62: TPanel
    Left = 248
    Top = 482
    Width = 33
    Height = 34
    Caption = '/'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 61
    OnClick = Panel53Click
  end
  object Panel63: TPanel
    Left = 248
    Top = 525
    Width = 33
    Height = 34
    Caption = '//'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 62
    OnClick = Panel53Click
  end
  object Panel64: TPanel
    Left = 248
    Top = 568
    Width = 33
    Height = 34
    Caption = '///'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 63
    OnClick = Panel53Click
  end
  object Panel65: TPanel
    Left = 457
    Top = 479
    Width = 33
    Height = 34
    Caption = '\'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 64
    OnClick = Panel53Click
  end
  object Panel66: TPanel
    Left = 457
    Top = 522
    Width = 33
    Height = 34
    Caption = '\\'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 65
    OnClick = Panel53Click
  end
  object Panel67: TPanel
    Left = 457
    Top = 565
    Width = 33
    Height = 34
    Caption = '\\\'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 66
    OnClick = Panel53Click
  end
  object Button1: TButton
    Left = 560
    Top = 16
    Width = 113
    Height = 41
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 67
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 560
    Top = 63
    Width = 113
    Height = 41
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 68
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 560
    Top = 110
    Width = 113
    Height = 41
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
    TabOrder = 69
    OnClick = Button3Click
  end
  object Panel68: TPanel
    Left = 24
    Top = 519
    Width = 33
    Height = 34
    Caption = 'X'
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 70
    OnClick = Panel53Click
  end
  object Panel69: TPanel
    Left = 24
    Top = 559
    Width = 33
    Height = 34
    Color = clSkyBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 71
    OnClick = Panel53Click
  end
  object Button4: TButton
    Left = 665
    Top = 255
    Width = 20
    Height = 106
    Caption = '>'
    TabOrder = 72
    OnClick = Button4Click
  end
  object Memo1: TMemo
    Left = 688
    Top = 16
    Width = 0
    Height = 546
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 73
    Visible = False
  end
  object Button5: TButton
    Left = 1004
    Top = 568
    Width = 113
    Height = 41
    Caption = #1048#1084#1087#1086#1088#1090
    TabOrder = 74
    Visible = False
    OnClick = Button5Click
  end
  object OpenDialog1: TOpenDialog
    Left = 568
    Top = 184
  end
  object SaveDialog1: TSaveDialog
    Left = 624
    Top = 184
  end
end
